import 'dart:async';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:get/get.dart';
import 'package:poc_camera/ui/app.dart';
import 'package:poc_camera/ui/controller/kyc_controller.dart';

List<CameraDescription> cameras;

Future<void> main() async {
  final KycController controller = Get.put(KycController());
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await availableCameras();
  controller.cameras = cameras;
  runApp(CameraApp());
}
