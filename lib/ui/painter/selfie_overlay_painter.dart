import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SelfieOverlayPainter extends CustomPainter {
  final scrimColor;

  SelfieOverlayPainter({
    this.scrimColor = Colors.black54,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);

    // Draw the semitransparent area with a cutout in center
    _paintViewScrim(canvas, size, center);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final previousPainter = oldDelegate as SelfieOverlayPainter;

    return (previousPainter.scrimColor != scrimColor);
  }

  void _paintViewScrim(Canvas canvas, Size canvasSize, Offset center) {
    final scrimPaint = Paint()..color = scrimColor;
    canvas.drawPath(
        Path.combine(
          PathOperation.difference,
          Path()
            ..addRect(Rect.fromLTWH(0, 0, canvasSize.width, canvasSize.height)),
          Path()
            ..addOval(Rect.fromLTWH(
              center.dx / 6,
              center.dy / 2.5,
              canvasSize.width - (center.dx / 3),
              canvasSize.height - center.dy / 1.3,
            ))
            ..close(),
        ),
        scrimPaint);
  }
}
