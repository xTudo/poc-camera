import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poc_camera/ui/pages/kyc_detail_page/kyc_detail_page.dart';

class CameraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: KycDetailPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
