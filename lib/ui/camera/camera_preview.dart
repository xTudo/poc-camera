import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';
import 'package:image/image.dart' as imglib;

import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:get/get.dart';
import 'package:poc_camera/ui/controller/kyc_controller.dart';

import 'image_preview.dart';

typedef convert_func = Pointer<Uint32> Function(
    Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, Int32, Int32, Int32, Int32);
typedef Convert = Pointer<Uint32> Function(
    Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, int, int, int, int);

class CameraPreviewWidget extends StatefulWidget {
  @override
  _CameraPreviewWidgetState createState() => _CameraPreviewWidgetState();
}

class _CameraPreviewWidgetState extends State<CameraPreviewWidget> {
  bool _cameraInitialized = false;
  KycController controller = Get.put(KycController());

  final DynamicLibrary convertImageLib = Platform.isAndroid
      ? DynamicLibrary.open("libconvertImage.so")
      : DynamicLibrary.process();
  Convert conv;

  @override
  void initState() {
    super.initState();
    _initializeCamera();

    // Load the convertImage() function from the library
    conv = convertImageLib
        .lookup<NativeFunction<convert_func>>('convertImage')
        .asFunction<Convert>();
  }

  void _initializeCamera() async {
    // Get list of cameras of the device
    // Create the CameraController
    print("aki");
    controller.cameraController =
        CameraController(controller.camera, ResolutionPreset.veryHigh);
    controller.cameraController.initialize().then((_) async {
      // Start ImageStream
      await controller.cameraController
          .startImageStream((CameraImage image) => _processCameraImage(image));
      setState(() {
        _cameraInitialized = true;
        controller.cameraOn = true;
      });
    });
  }

  void _processCameraImage(CameraImage image) async {
    setState(() {
      // _savedImage = image;
      controller.savedImage = image;
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller.cameraController.dispose();
    _cameraInitialized = false;
  }

  @override
  Widget build(BuildContext context) {
    if (!_cameraInitialized) return CircularProgressIndicator();
    return SafeArea(
        child: Scaffold(
      body: Transform.scale(
          scale: 1 / controller.cameraController.value.aspectRatio,
          child: CameraPreview(controller.cameraController)),
    ));
  }
}
