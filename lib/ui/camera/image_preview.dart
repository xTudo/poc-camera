import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image/image.dart' as imglib;
import 'package:poc_camera/ui/controller/kyc_controller.dart';

class ImagePreview extends StatelessWidget {
  final KycController controller = Get.put(KycController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Column(
      children: [
        Expanded(
            child: RotatedBox(
              quarterTurns: controller.step.value == 2 ? 2 : 0,
              child: Image.memory(imglib.encodeJpg(controller.img),
                  fit: BoxFit.fitHeight),
            ))
      ],
    )));
  }
}
