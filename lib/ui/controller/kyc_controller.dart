import 'package:camera/camera.dart';
import 'package:get/get.dart';

class KycController extends GetxController {
  var camerasList = [].obs;
  CameraController cameraController;
  var cameraIndex = 0.obs;

  get camera => camerasList[cameraIndex.value];
  var documentFrontPicPathValue = "".obs;
  var documentBackPicPathValue = "".obs;
  var selfiePicPathValue = "".obs;
  var _cameraOnValue = true.obs;
  var picRecent;
  var imgRecent;
  var img;
  var savedImage;
  var conv;
  var step = 0.obs;

  nextStep() {
    step.value++;
    if (step.value == 2 && camerasList.length > 1) cameraIndex.value = 1;
    update();
  }

  set cameraOn(value) {
    _cameraOnValue.value = value;
    update();
  }

  set cameras(value) {
    for (var x in value) {
      camerasList.add(x);
    }
    update();
  }

  set documentFrontPicPath(value) {
    print("\n\n\n $value \n\n\n");
    documentFrontPicPathValue.value = value;
    picRecent.value = value;
    _cameraOnValue.value = false;
    update();
  }

  set documentBackPicPath(value) {
    documentBackPicPathValue.value = value;
    picRecent.value = value;
    _cameraOnValue.value = false;
    update();
  }

  set selfiePicPath(value) {
    selfiePicPathValue.value = value;
    picRecent.value = value;
    _cameraOnValue.value = false;
    update();
  }

  get documentFrontPicPath => documentFrontPicPathValue.value;

  get documentBackPicPath => documentBackPicPathValue.value;

  get selfiePicPath => selfiePicPathValue.value;

  get cameras => camerasList;

  get cameraOn => _cameraOnValue.value;

  String get labelText {
    print(step.value);
    switch (step.value) {
      case 0:
        print("aki");
        // print(documentFrontPicPathValue.value);
        return "Tire uma foto da frente do seu documento de identidade ou CNH";
      case 1:
        return "Tire uma foto do verso do seu documento de identidade ou CNH";
      default:
        return "Tire uma foto do seu rosto segurando seu documento de identidade ou CNH";
    }
  }

  RxBool get hasPicture {
    print(step.value);
    switch (step.value) {
      case 0:
        print("aki");
        // print(documentFrontPicPathValue.value);
        return documentFrontPicPath.length() > 0;
      case 1:
        return documentBackPicPath.length() > 0;
      default:
        return selfiePicPath.length() > 0;
    }
  }

  void savePicPath(value) {
    print(value);
    if (documentFrontPicPath == "")
      documentFrontPicPathValue.value = value;
    else if (documentBackPicPath == "")
      documentBackPicPathValue.value = value;
    else
      selfiePicPathValue.value = value;

    picRecent = value;
    cameraOn = false;
    update();
  }

  void resetPic() {
    if (documentFrontPicPath == picRecent)
      documentFrontPicPathValue.value = "";
    else if (documentBackPicPath == picRecent)
      documentBackPicPathValue.value = "";
    else
      selfiePicPathValue.value = "";
    cameraOn = true;
    update();
  }
}
