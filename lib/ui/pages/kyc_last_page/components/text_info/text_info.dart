import 'package:flutter/material.dart';

class TextInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.only(
            left: 4.722222133919044 * widthMultiplier,
            right: 4.722222133919044 * widthMultiplier,
            top: 5.316930676222444 * heightMultiplier,
          ),
          alignment: Alignment.topCenter,
          child: Text(
            "Nossa equipe está " +
                "analisando suas " +
                "informações.",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 3.721851473355711 * heightMultiplier,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              left: 7.083333200878567 * widthMultiplier,
              right: 7.083333200878567 * widthMultiplier,
              top: 2.1267722704889778 * heightMultiplier),
          alignment: Alignment.topCenter,
          child: Text(
            "Essa análise é necessária para a " +
                "liberação do uso de sua conta para que " +
                "você utilize com um máximo nível de " +
                "segurança.",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 2.1267722704889778 * heightMultiplier,
              color: Colors.black54,
              // fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
