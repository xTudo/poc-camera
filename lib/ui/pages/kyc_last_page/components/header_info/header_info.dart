import 'package:flutter/material.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/widgets/pic_header/pic_header.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/components/text_info/text_info.dart';

class HeaderInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PicHeader(),
        TextInfo(),
      ],
    );
  }
}
