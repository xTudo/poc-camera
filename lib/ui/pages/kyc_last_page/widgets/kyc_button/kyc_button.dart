import 'package:flutter/material.dart';
import 'package:poc_camera/ui/widgets/button_widget/button_widget.dart';

class KycButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return Container(
      padding: EdgeInsets.only(
        right: 16.527777468716657 * widthMultiplier,
        top: 4.222972972972973 * heightMultiplier,
        left: 16.527777468716657 * widthMultiplier,
      ),
      // height: 50,
      // width: size.width / 1.5,
      child: ButtonWidget(
          onTap: () => print("Fim"),
          borderRadius: 0.5316930676222444 * heightMultiplier,

          padding: EdgeInsets.symmetric(
              vertical: 1.0633861352444889 * heightMultiplier),

          child: Text(
            "OK. Entendi.",
            style: TextStyle(
                color: Colors.white,
                fontSize: 2.1267722704889778 * heightMultiplier,
                fontWeight: FontWeight.bold),
          )),
    );
  }
}
