import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PicHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    return Container(
        alignment: Alignment.center,
        child: SvgPicture.asset(
          "assets/kyc/conta-verificada.svg",
          fit: BoxFit.fitHeight,
          height: 31.901584057334667 * heightMultiplier,
        ),
    );
  }
}
