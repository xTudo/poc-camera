import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/components/header_info/header_info.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/widgets/kyc_button/kyc_button.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/widgets/pic_header/pic_header.dart';
import 'package:poc_camera/ui/widgets/button_widget/button_widget.dart';

class KycLastPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          HeaderInfo(),
          KycButtonWidget(),
        ],
      ),
    ));
  }
}
