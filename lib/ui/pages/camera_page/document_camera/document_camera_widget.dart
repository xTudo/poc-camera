import 'package:flutter/material.dart';

import 'package:poc_camera/ui/camera/camera_preview.dart';
import 'package:poc_camera/ui/camera/image_preview.dart';
import 'package:poc_camera/ui/controller/kyc_controller.dart';
import 'package:poc_camera/ui/painter/document_overlay_painter.dart';
import 'package:poc_camera/ui/painter/selfie_overlay_painter.dart';
import 'package:poc_camera/ui/widgets/button_widget/button_widget.dart';
import 'package:poc_camera/ui/widgets/take_picture_button.dart';
import 'package:get/get.dart';

class DocumentCameraWidget extends StatelessWidget {
  final KycController controller = Get.put(KycController());
  Size size;
  double heightMultiplier;
  double widthMultiplier;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    heightMultiplier = size.height / 100;
    widthMultiplier = size.width / 100;
    // KycController.cameraIndex = 0;
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Obx(() =>
              controller.cameraOn ? CameraPreviewWidget() : ImagePreview()),
          _getCustomPainter(),
          Container(
              alignment: Alignment.topCenter,
              padding: EdgeInsets.symmetric(
                  horizontal: 3.3783783783783785 * widthMultiplier,
                  vertical: 5.56 * heightMultiplier),
              child: Obx(
                () => Text(
                  controller.labelText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 3.721851473355711 * heightMultiplier,
                  ),
                ),
              )),
          Container(
            alignment: Alignment.bottomCenter,
            child: _getButton(),
          )
        ],
      ),
    );
  }

  Widget _getCustomPainter() {
    return Obx(() => controller.step.value == 2
        ? _selfieCustomPainter()
        : _documentCustomPainter());
  }

  Widget _documentCustomPainter() => CustomPaint(
        size: size,
        painter: DocumentOverlayPainter(
          scrimColor: Colors.black54,
          drawBorder: false,
          viewHeight: 53.169306762224444 * heightMultiplier,
          viewWidth: 70.83333200878567 * widthMultiplier,
        ),
      );

  Widget _selfieCustomPainter() => CustomPaint(
        size: size,
        painter: SelfieOverlayPainter(
          scrimColor: Colors.black54,
        ),
      );

  Widget _getButton() => Obx(
        () => Padding(
            padding: EdgeInsets.symmetric(
                vertical: 5.316930676222444 * heightMultiplier,
                horizontal: 4.722222133919044 * widthMultiplier),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: controller.cameraOn
                  ? [Expanded(child: TakePictureButton())]
                  : [
                      Expanded(
                        flex: 45,
                        child: ButtonWidget(
                          backgroundColor: Colors.white,
                          onTap: () => controller.resetPic(),
                          borderRadius: 0.5316930676222444 * heightMultiplier,
                          padding: EdgeInsets.symmetric(
                              vertical: 1.0633861352444889 * heightMultiplier),
                          child: Text(
                            "TIRAR FOTO",
                            style: TextStyle(
                                // color: Colors.white,
                                fontSize: 2.1267722704889778 * heightMultiplier,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 10,
                        child: SizedBox.shrink(),
                      ),
                      Expanded(
                        flex: 45,
                        child: ButtonWidget(
                          onTap: () {
                            controller.nextStep();
                            controller.cameraOn = true;
                          },
                          borderRadius: 0.5316930676222444 * heightMultiplier,
                          padding: EdgeInsets.symmetric(
                              vertical: 1.0633861352444889 * heightMultiplier),
                          child: Text(
                            "PROSSEGUIR",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 2.1267722704889778 * heightMultiplier,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
            )),
      );
}
