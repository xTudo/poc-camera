import 'package:flutter/material.dart';
import 'package:poc_camera/ui/controller/kyc_controller.dart';
import 'package:poc_camera/ui/pages/camera_page/document_camera/document_camera_widget.dart';
import 'package:get/get.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/kyc_last_page.dart';

class CameraPage extends StatelessWidget {
  final KycController controller = Get.put(KycController());
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(body: _getPage()),
    );
  }

  _getPage() {
    return Obx(() {
      if (controller.step >= 3) return KycLastPage();
      return DocumentCameraWidget();
    });
  }
}
