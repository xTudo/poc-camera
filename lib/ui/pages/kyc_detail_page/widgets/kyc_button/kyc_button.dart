import 'package:flutter/material.dart';
import 'package:poc_camera/ui/pages/camera_page/camera_page.dart';
import 'package:poc_camera/ui/pages/kyc_last_page/kyc_last_page.dart';
import 'package:poc_camera/ui/widgets/button_widget/button_widget.dart';

class KycButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return Container(
      padding: EdgeInsets.only(
        right: 16.527777468716657 * widthMultiplier,
        bottom: 10.633861352444889 * heightMultiplier,
        left: 16.527777468716657 * widthMultiplier,
      ),
      child: ButtonWidget(
          onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CameraPage(),
                ),
              ),
          borderRadius: 0.5316930676222444 * heightMultiplier,
          padding: EdgeInsets.symmetric(
              vertical: 1.0633861352444889 * heightMultiplier),
          child: Text(
            "COMEÇAR",
            style: TextStyle(
                color: Colors.white,
                fontSize: 2.1267722704889778 * heightMultiplier,
                fontWeight: FontWeight.bold),
          )),
    );
  }
}
