import 'package:flutter/material.dart';
import 'package:poc_camera/ui/pages/kyc_detail_page/components/kyc_detail_alerts/kyc_detail_alerts.dart';
import 'package:poc_camera/ui/pages/kyc_detail_page/components/kyc_detail_bottom/kyc_detail_bottom.dart';
import 'package:poc_camera/ui/pages/kyc_detail_page/components/kyc_detail_header/kyc_detail_header.dart';
import 'package:poc_camera/ui/pages/kyc_detail_page/widgets/kyc_button/kyc_button.dart';

class KycDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          KycDetailHeader(),
          KycDetailAlerts(),
          KycDetailBottom(),
          KycButtonWidget(),
        ],
      ),
    ));
  }
}
