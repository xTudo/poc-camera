import 'package:flutter/material.dart';

class KycDetailBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return Expanded(
      flex: 10,
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: 7.083333200878567 * widthMultiplier),
            // vertical: 2.1267722704889778 * heightMultiplier),
        alignment: Alignment.topCenter,
        child: Text(
          "Se for necessário, podemos compartilhar suas " +
              "imagens com instituições e empresas parceiras " +
              "para validação de identidade.",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 2.1267722704889778 * heightMultiplier,
            color: Colors.black54,
            // fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
