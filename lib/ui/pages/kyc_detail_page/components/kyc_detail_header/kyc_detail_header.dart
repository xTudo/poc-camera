import 'package:flutter/material.dart';

class KycDetailHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return Expanded(
        flex: 25,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(
                  left: 4.722222133919044 * widthMultiplier,
                  right: 4.722222133919044 * widthMultiplier,
                  top: 5.316930676222444 * heightMultiplier,
              ),
              alignment: Alignment.topCenter,
              child: Text(
                "Fase final! Envie seus documentos para análise.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 3.721851473355711 * heightMultiplier,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: 7.083333200878567 * widthMultiplier,
                  right: 7.083333200878567 * widthMultiplier,
                  top: 2.1267722704889778 * heightMultiplier),
              alignment: Alignment.topCenter,
              child: Text(
                "Para a segurança da sua carteira, " +
                    "precisamos validar sua identidade, " +
                    "por isso o envio de imagens é necessária.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 2.1267722704889778 * heightMultiplier,
                  color: Colors.black54,
                  // fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ));
  }
}
