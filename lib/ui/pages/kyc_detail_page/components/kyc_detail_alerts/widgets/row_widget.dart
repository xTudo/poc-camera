import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RowWidget extends StatelessWidget {
  final pic;
  final label;
  RowWidget({@required this.pic, @required this.label});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 11.805555334797612 * widthMultiplier),
      child: Row(
        children: [
          SvgPicture.asset(
            pic,
            height: 4.2535445409779555 * heightMultiplier,
            fit: BoxFit.fitHeight,
          ),
          Expanded(
              child: Padding(
            padding: EdgeInsets.only(left: 4.722222133919044 * widthMultiplier),
            child: Text(
              label,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 3.1901584057334667 * heightMultiplier,
                color: Colors.black54,
                // fontWeight: FontWeight.bold,
              ),
            ),
          ))
        ],
      ),
    );
  }
}
