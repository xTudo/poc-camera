import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:poc_camera/ui/pages/kyc_detail_page/components/kyc_detail_alerts/widgets/row_widget.dart';

class KycDetailAlerts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    return Expanded(
        flex: 30,
        child: Column(
          children: [
            RowWidget(
                pic: "assets/kyc/idea.svg", label: "Atenção com a iluminação."),
            Divider(
              height: 2.1267722704889778 * heightMultiplier,
              color: Colors.transparent,
            ),
            RowWidget(
                pic: "assets/kyc/sharp.svg",
                label: "Verifique se a foto está nítida."),
            Divider(
              height: 2.1267722704889778 * heightMultiplier,
              color: Colors.transparent,
            ),
            RowWidget(
                pic: "assets/kyc/sorrir.svg",
                label: "Expressão neutra é uma boa idéia."),
            Divider(
              height: 2.1267722704889778 * heightMultiplier,
              color: Colors.transparent,
            ),
            RowWidget(
                pic: "assets/kyc/anonimo.svg",
                label: "Selfie tem que ser sem acessórios."),
          ],
        ));
  }
}
