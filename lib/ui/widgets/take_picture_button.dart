import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poc_camera/ui/controller/kyc_controller.dart';
import 'package:image/image.dart' as imglib;
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:poc_camera/ui/widgets/button_widget/button_widget.dart';

typedef Convert = Pointer<Uint32> Function(
    Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, int, int, int, int);
typedef convert_func = Pointer<Uint32> Function(
    Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, Int32, Int32, Int32, Int32);

// ignore: must_be_immutable
class TakePictureButton extends StatelessWidget {
  final KycController controller = Get.put(KycController());
  final DynamicLibrary convertImageLib = Platform.isAndroid
      ? DynamicLibrary.open("libconvertImage.so")
      : DynamicLibrary.process();
  Convert conv;

  TakePictureButton() {
    conv = convertImageLib
        .lookup<NativeFunction<convert_func>>('convertImage')
        .asFunction<Convert>();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    return ButtonWidget(
      backgroundColor: Colors.white,
      onTap: () => _takePicture(context),
      borderRadius: 0.5316930676222444 * heightMultiplier,
      padding:
          EdgeInsets.symmetric(vertical: 1.0633861352444889 * heightMultiplier),
      child: Text(
        "TIRAR FOTO",
        style: TextStyle(
            // color: Colors.white,
            fontSize: 2.1267722704889778 * heightMultiplier,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  void _takePicture(context) async {
    imglib.Image img;
    if (Platform.isAndroid) {
      // Allocate memory for the 3 planes of the image
      Pointer<Uint8> p =
          allocate(count: controller.savedImage.planes[0].bytes.length);
      Pointer<Uint8> p1 =
          allocate(count: controller.savedImage.planes[1].bytes.length);
      Pointer<Uint8> p2 =
          allocate(count: controller.savedImage.planes[2].bytes.length);

      // Assign the planes data to the pointers of the image
      Uint8List pointerList =
          p.asTypedList(controller.savedImage.planes[0].bytes.length);
      Uint8List pointerList1 =
          p1.asTypedList(controller.savedImage.planes[1].bytes.length);
      Uint8List pointerList2 =
          p2.asTypedList(controller.savedImage.planes[2].bytes.length);
      pointerList.setRange(0, controller.savedImage.planes[0].bytes.length,
          controller.savedImage.planes[0].bytes);
      pointerList1.setRange(0, controller.savedImage.planes[1].bytes.length,
          controller.savedImage.planes[1].bytes);
      pointerList2.setRange(0, controller.savedImage.planes[2].bytes.length,
          controller.savedImage.planes[2].bytes);

      // Call the convertImage function and convert the YUV to RGB
      Pointer<Uint32> imgP = conv(
          p,
          p1,
          p2,
          controller.savedImage.planes[1].bytesPerRow,
          controller.savedImage.planes[1].bytesPerPixel,
          controller.savedImage.planes[0].bytesPerRow,
          controller.savedImage.height);

      // Get the pointer of the data returned from the function to a List
      List imgData = imgP.asTypedList(
          (controller.savedImage.planes[0].bytesPerRow *
              controller.savedImage.height));
      // Generate image from the converted data
      img = imglib.Image.fromBytes(controller.savedImage.height,
          controller.savedImage.planes[0].bytesPerRow, imgData);

      // Free the memory space allocated
      // from the planes and the converted data
      free(p);
      free(p1);
      free(p2);
      free(imgP);
    } else if (Platform.isIOS) {
      img = imglib.Image.fromBytes(
        controller.savedImage.planes[0].bytesPerRow,
        controller.savedImage.height,
        controller.savedImage.planes[0].bytes,
        format: imglib.Format.bgra,
      );
    }

    final String path = join(
      // Store the picture in the temp directory.
      // Find the temp directory using the `path_provider` plugin.
      (await getTemporaryDirectory()).path,
      '${DateTime.now()}.png',
    );

    print("PATH = $path");
    // controller.imgRecent = path;
    controller.savePicPath(path);
    controller.img = img;
  }
}
