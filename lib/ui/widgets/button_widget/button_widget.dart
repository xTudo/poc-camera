import 'package:flutter/material.dart';

// import '../../../themes/theme.dart';
// import '../../../utils/size_config.dart';

class ButtonWidget extends StatelessWidget {
  final Color backgroundColor;
  final Color foregroundColor;
  final Color splashColor;
  final double borderRadius;
  final double height;
  final double width;
  final Function onTap;
  final Alignment alignment;
  final Widget child;
  final EdgeInsets padding;

  const ButtonWidget(
      {Key key,
      @required this.child,
      this.backgroundColor,
      this.foregroundColor,
      this.borderRadius,
      this.height,
      this.onTap,
      this.padding,
      this.width,
      this.alignment,
      this.splashColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var heightMultiplier = size.height / 100;
    var widthMultiplier = size.width / 100;
    print(40 / widthMultiplier);
    print(20 / heightMultiplier);
    print(20 / widthMultiplier);
    return Container(
      child: Material(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 0),
        ),
        color: Colors.transparent,
        child: Card(
          elevation: 3,
          margin: EdgeInsets.zero,
          color: backgroundColor ?? Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 0),
          ),
          child: InkWell(
            // splashColor: splashColor ?? MainTheme.splashColor,
            borderRadius: BorderRadius.circular(borderRadius ?? 0),
            onTap: onTap,
            child: Container(
              padding: padding ??
                  EdgeInsets.symmetric(
                    vertical: 1.836659 * heightMultiplier,
                    horizontal: 3.8197 * widthMultiplier,
                  ),
              height: height ?? 6.1222 * heightMultiplier,
              width: width,
              alignment: alignment ?? Alignment.center,
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
